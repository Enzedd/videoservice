Start tests
```bash
docker‑compose up autotests
```

Start django runserver
```bash
docker‑compose up runserver
```
Open app in http://127.0.0.1:8000

Create admin user
```bash
docker exec -it vs_runserver python manage.py createsuperuser --username admin --email admin@example.com
```

Notes:
1. celery is installed from github as latest release version has python 3.7 compatibility issues.