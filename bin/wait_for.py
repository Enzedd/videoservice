# coding=utf-8
import socket
import sys
import time

__author__ = 'Niyaz.Batyrshin'

# TODO timeout
if __name__ == '__main__':
    host, port = sys.argv[1].split(':')
    port = int(port)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(f'Waiting for {host}:{port}')
    while True:
        try:
            s.connect((host, port))
            s.close()
            print(f'{host}:{port} is up and running')
            break
        except socket.error as ex:
            time.sleep(.1)
