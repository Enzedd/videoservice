# coding=utf-8
import logging

from rest_framework.routers import SimpleRouter

from videosvc.views import PageViewSet

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)

router = SimpleRouter()
router.register(r'page', PageViewSet, base_name='page')
urlpatterns = router.urls
