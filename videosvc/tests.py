# coding=utf-8
import logging

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from videosvc.models import Audio, ContentItem, Page, Text, Video

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)


class TestPageViewSet(APITestCase):
    def setUp(self) -> None:
        ContentItem.objects.all().delete()
        Page.objects.all().delete()

    def test_retrieve(self):
        page1 = Page.objects.create(title='Page1')
        text = Text.objects.create(page=page1, title='Text1', body='Text body')
        video = Video.objects.create(page=page1, title='Video1', file_name='video.avi')
        audio = Audio.objects.create(page=page1, title='Audio1', file_name='audio.mp3', bit_rate=320)

        with self.settings(CELERY_TASK_ALWAYS_EAGER=True):
            response = self.client.get(reverse('page-detail', args=(page1.id,)), format='json')

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data['id'], page1.id)
            self.assertEqual(response.data['title'], page1.title)
            self.assertEqual(len(response.data['content_items']), 3)

        text.refresh_from_db()
        self.assertEqual(text.counter, 1)
        video.refresh_from_db()
        self.assertEqual(video.counter, 1)
        audio.refresh_from_db()
        self.assertEqual(audio.counter, 1)

    def test_list(self):
        page1 = Page.objects.create(title='Page1')

        response = self.client.get(reverse('page-list'), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(len(response.data['results']), 1)
        self.assertIsNotNone(response.data['results'][0]['url'])
