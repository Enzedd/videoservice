from typing import List

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import CASCADE


class Page(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class ContentItem(models.Model):
    page = models.ForeignKey('Page', on_delete=CASCADE, related_name='content_items')
    title = models.CharField(max_length=255)
    counter = models.PositiveIntegerField(default=0)
    order = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.title

    @classmethod
    def sub_models(cls) -> List[str]:
        return [model._meta.model_name for model in cls.__subclasses__()]


class Text(ContentItem):
    body = models.TextField()


class Video(ContentItem):
    file_name = models.FileField()
    subtitles_file_name = models.FileField(null=True, blank=True)


class Audio(ContentItem):
    file_name = models.FileField()
    bit_rate = models.PositiveIntegerField()
