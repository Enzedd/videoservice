# coding=utf-8
import logging

from celery import current_app
from django.db.models import F

from videosvc.models import ContentItem

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)


@current_app.task(bind=True)
def increment_view_count(self, page_id):
    ContentItem.objects.filter(page_id=page_id).update(counter=F('counter') + 1)
