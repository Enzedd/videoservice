from django.db.models import Prefetch
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from videosvc.models import ContentItem, Page
from videosvc.serializers import PageListSerializer, PageSerializer
from videosvc.tasks import increment_view_count


class PageViewSet(mixins.RetrieveModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return PageListSerializer
        return super().get_serializer_class()

    def get_queryset(self):
        qs = super().get_queryset()
        if self.action == 'retrieve':
            qs = qs.prefetch_related(Prefetch('content_items', ContentItem.objects.select_related(*ContentItem.sub_models()).order_by('order')))
        return qs

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        pk = self.kwargs[lookup_url_kwarg]
        increment_view_count.apply_async(args=(pk,))

        return response
