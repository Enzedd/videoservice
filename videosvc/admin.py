from django.contrib import admin

from videosvc.models import Audio, ContentItem, Page, Text, Video


class ContentItemInline(admin.TabularInline):
    model = ContentItem
    readonly_fields = ('counter',)

    show_change_link = True
    extra = 0
    ordering = ('order',)


class TextInline(ContentItemInline):
    model = Text


class VideoInline(ContentItemInline):
    model = Video


class AudioInline(ContentItemInline):
    model = Audio


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    inlines = (
        TextInline,
        VideoInline,
        AudioInline,
    )
    search_fields = ('title__startswith', 'content_items__title__startswith')
