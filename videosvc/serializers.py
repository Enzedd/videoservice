# coding=utf-8
import logging

from rest_framework import serializers

from videosvc.models import Audio, ContentItem, Page, Text, Video

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)


class ContentItemSerializer(serializers.ModelSerializer):
    content_item_type = serializers.SerializerMethodField()

    def get_content_item_type(self, obj):
        return obj._meta.model_name

    class Meta:
        model = ContentItem
        exclude = ('page',)


class TextSerializer(ContentItemSerializer):
    class Meta(ContentItemSerializer.Meta):
        model = Text


class AudioSerializer(ContentItemSerializer):
    class Meta(ContentItemSerializer.Meta):
        model = Audio


class VideoSerializer(ContentItemSerializer):
    class Meta(ContentItemSerializer.Meta):
        model = Video


class GenericContentItemSerializer(serializers.ModelSerializer):
    content_item_serializers = {
        'text': TextSerializer(),
        'audio': AudioSerializer(),
        'video': VideoSerializer(),
    }

    def to_representation(self, instance):
        for attr_name in self.content_item_serializers.keys():
            if hasattr(instance, attr_name):
                item_type = attr_name
                _instance = getattr(instance, item_type)
                serializer = self.content_item_serializers.get(item_type, None)
                return serializer.to_representation(_instance)
        return ContentItemSerializer(instance)


class PageSerializer(serializers.ModelSerializer):
    content_items = GenericContentItemSerializer(many=True, read_only=True)

    class Meta:
        model = Page
        exclude = ()


class PageListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('url',)
