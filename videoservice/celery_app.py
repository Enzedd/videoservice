# coding=utf-8
import logging
import os

from celery import Celery

__author__ = 'Niyaz.Batyrshin'
logger = logging.getLogger(__name__)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'videoservice.settings')

app = Celery('videoservice')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
