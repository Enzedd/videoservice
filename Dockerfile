FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements /code/
RUN pip install -r requirements
COPY . /code/
VOLUME /var/lib/videoservice/media
VOLUME /var/lib/videoservice/static
VOLUME /var/log/videoservice
